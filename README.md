# Azure Kubernetes

## Criar permissoes de acesso para a API Azure

Para integração do Kubernetes com a Azure é necessária a configração de um App registrations,
com o **appId** e **Password** gerados abaixo serão usados no passo seguinte:

``` bash
az ad app create --display-name k8s-cluster --identifier-uris http://k8s --password <Password>
az ad sp create --id <appId>
az role assignment create --role "Owner" --assignee <appId> --scope /subscriptions/<subscriptionId>
```

* https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-create-service-principal-portal

## Arquivo do Cloud Provider

## Ajustar configurações do kube-controller para interação com a Azure

Deve ser adicionados os parametros abaixo para o executável do kube-controller, as
alterações estão no arquivo de patch:

```bash
kubectl patch -f /etc/kubernetes/manifests/kube-controller-manager.yaml \
              --local --type='json' -p "$(cat kube-controller-patch.json)" -o yaml
```
