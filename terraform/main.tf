provider "azurerm" {}

module "k8s_master" {
  source = "./modules/vm/"

  count         = "1"
  computer_name = "k8s-master"
  environment   = "${var.environment}"
  company       = "${var.company}"

  resource_group    = "${var.resource_group}"
  vnet              = "${var.vnet}"
  public_key        = "${var.public_key}"
  subnets           = ["${var.subnet}"]
  vm_public_ip      = "${var.vm_public_ip}"
  vm_sg_allow_ssh   = "${var.vm_sg_allow_ssh}"
  vm_data_disk      = "${var.vm_data_disk}"
  vm_data_disk_size = "20"
  vm_size           = "${var.node_vm_size}"
  location          = "${var.region}"

  role = "k8s-master"
}

module "k8s_node" {
  source = "./modules/vm/"

  count               = "${var.node_count}"
  computer_name       = "k8s-node"
  environment         = "${var.environment}"
  company             = "${var.company}"
  vm_availability_set = "${azurerm_availability_set.k8s_as.id}"
  location          = "${var.region}"

  resource_group  = "${var.resource_group}"
  vnet            = "${var.vnet}"
  public_key      = "${var.public_key}"
  subnets         = ["${var.subnet}"]
  vm_public_ip    = "${var.vm_public_ip}"
  vm_sg_allow_ssh = "${var.vm_sg_allow_ssh}"
  vm_data_disk    = "${var.vm_data_disk}"
  vm_size         = "${var.master_vm_size}"

  role = "k8s-node"
}

data "template_file" "config_ansible" {
  template = "${file("files/config_ansible.yml")}"

  vars {
    tenantId          = "${var.azure_tenantId}"
    subscriptionId    = "${var.azure_subscriptionId}"
    aadClientId       = "${var.azure_aadClientId}"
    aadClientSecret   = "${var.azure_aadClientSecret}"
    resourceGroup     = "${var.resource_group}"
    location          = "${var.region}"
    securityGroupName = "${module.k8s_node.azurerm_network_security_group_name}"
    vnet              = "${var.vnet}"
    subnetName        = "${var.subnet}"
    routeTableName    = "${var.route_table}"

    kubernetes_version = "${var.kubernetes_version}"
  }
}

resource "null_resource" "local" {
  triggers {
    template = "${data.template_file.config_ansible.rendered}"
  }

  provisioner "local-exec" {
    command = "echo \"${data.template_file.config_ansible.rendered}\" > ../ansible/inventories/azure/group_vars/k8s-cluster.yml"
  }
}

resource "azurerm_availability_set" "k8s_as" {
  name                = "k8s_as"
  location          = "${var.region}"
  resource_group_name = "${var.resource_group}"
  managed = true

  tags {
    environment = "${var.environment}"
  }
}
