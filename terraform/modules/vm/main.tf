resource "azurerm_virtual_machine" "virtual_machine" {
  count                            = "${var.count}"
  name                             = "${var.company}-${var.environment}-${var.computer_name}-${count.index}"
  location                         = "${var.location}"
  resource_group_name              = "${data.azurerm_resource_group.rg.name}"
  network_interface_ids            = ["${azurerm_network_interface.network_interface.*.id[count.index]}"]
  vm_size                          = "${var.vm_size}"
  delete_os_disk_on_termination    = "${var.delete_os_disk_on_termination}"
  delete_data_disks_on_termination = "${var.delete_data_disks_on_termination}"
  availability_set_id              = "${var.vm_availability_set}"

  storage_image_reference {
    publisher = "${lookup(var.images, "${var.vm_distribution}.publisher")}"
    offer     = "${lookup(var.images, "${var.vm_distribution}.offer")}"
    sku       = "${lookup(var.images, "${var.vm_distribution}.sku")}"
    version   = "latest"
  }

  storage_os_disk {
    name          = "${var.computer_name}-${count.index}-os-disk"
    caching       = "ReadWrite"
    create_option = "FromImage"
  }

  os_profile {
    computer_name  = "${var.company}-${var.environment}-${var.computer_name}-${count.index}"
    admin_username = "${var.admin_username}"
    custom_data    = "${var.vm_custom_data}"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys = [{
      path     = "/home/${var.admin_username}/.ssh/authorized_keys"
      key_data = "${var.public_key}"
    }]
  }

  tags {
    environment = "${var.environment}"
    role        = "${var.role}"
  }
}

data "azurerm_resource_group" "rg" {
  name = "${var.resource_group}"
}

data "azurerm_subnet" "subnets" {
  count                = "${length(var.subnets)}"
  name                 = "${element(var.subnets, count.index)}"
  virtual_network_name = "${var.vnet}"
  resource_group_name  = "${var.resource_group}"
}

resource "azurerm_network_interface" "network_interface" {
  count                     = "${var.count}"
  name                      = "${var.computer_name}-${count.index}-nic"
  location                  = "${var.location}"
  resource_group_name       = "${data.azurerm_resource_group.rg.name}"
  network_security_group_id = "${azurerm_network_security_group.sg.id}"

  ip_configuration {
    name                          = "${var.computer_name}-${count.index}-ip"
    subnet_id                     = "${element(data.azurerm_subnet.subnets.*.id, count.index)}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${var.vm_public_ip ? element(concat(azurerm_public_ip.pubip.*.id, list("")), count.index) : ""}"
  }

  tags {
    environment = "${var.environment}"
  }
}

resource "azurerm_public_ip" "pubip" {
  name                         = "${var.computer_name}-${count.index}-pubip"
  location                     = "${var.location}"
  resource_group_name          = "${data.azurerm_resource_group.rg.name}"
  public_ip_address_allocation = "static"
  count                        = "${var.vm_public_ip ? var.count : 0}"

  tags {
    environment = "${var.environment}"
  }
}

resource "azurerm_network_security_group" "sg" {
  name                = "${var.computer_name}-sg"
  location            = "${var.location}"
  resource_group_name = "${data.azurerm_resource_group.rg.name}"
}

resource "azurerm_network_security_rule" "rule_egress" {
  name                        = "${var.computer_name}-egress"
  description                 = "Allow Outbound access"
  priority                    = 100
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${data.azurerm_resource_group.rg.name}"
  network_security_group_name = "${azurerm_network_security_group.sg.name}"
}

resource "azurerm_network_security_rule" "rule_vnet_local" {
  name                        = "${var.computer_name}-vnet"
  count                       = "${var.vm_sg_allow_vnet ? 1 : 0}"
  description                 = "Allow VNet internal network access"
  priority                    = 1000
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "VirtualNetwork"
  destination_address_prefix  = "VirtualNetwork"
  resource_group_name         = "${data.azurerm_resource_group.rg.name}"
  network_security_group_name = "${azurerm_network_security_group.sg.name}"
}

resource "azurerm_network_security_rule" "rule_ssh" {
  name                        = "${var.computer_name}-allow-ssh"
  count                       = "${var.vm_sg_allow_ssh ? 1 : 0}"
  description                 = "Allow SSH access"
  priority                    = 200
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "0.0.0.0/0"
  destination_address_prefix  = "0.0.0.0/0"
  resource_group_name         = "${data.azurerm_resource_group.rg.name}"
  network_security_group_name = "${azurerm_network_security_group.sg.name}"
}

resource "azurerm_managed_disk" "data" {
  name                 = "${var.computer_name}-${count.index}-data-disk"
  count                = "${var.vm_data_disk ? var.count : 0}"
  location             = "${var.location}"
  resource_group_name  = "${data.azurerm_resource_group.rg.name}"
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = "${var.vm_data_disk_size}"
  # zones                = [ "${compact(list(var.vm_availability_set))}" ] 
}

resource "azurerm_virtual_machine_data_disk_attachment" "data" {
  count              = "${var.vm_data_disk ? var.count : 0}"
  managed_disk_id    = "${azurerm_managed_disk.data.*.id[count.index]}"
  virtual_machine_id = "${azurerm_virtual_machine.virtual_machine.*.id[count.index]}"
  lun                = "10"
  caching            = "ReadWrite"
}
