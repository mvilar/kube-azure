output "azurerm_network_security_group_id" {
  value = "${azurerm_network_security_group.sg.id}"
}

output "azurerm_network_security_group_name" {
  value = "${azurerm_network_security_group.sg.name}"
}
