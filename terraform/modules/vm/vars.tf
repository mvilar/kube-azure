variable "resource_group" {
  description = "Nome do Resource Group"
}

variable "location" {
  description = "Regiao da Azure"
  default = "eastus"
}


variable "vnet" {
  description = "Nome da VNet"
}

variable "public_key" {
  description = "Public Key para o usuario admin"
}

variable "subnets" {
  description = "Nomes das Subnets"
  default     = []
}

variable "environment" {
  default = "dev"
}

variable "role" {
  default = ""
}

variable "company" {
  default = "azure"
}

variable "computer_name" {
  description = "VM name"
  default     = "vm"
}

variable "count" {
  description = "Quantidade de VMs a ser criada"
  default     = "1"
}

variable "admin_username" {
  default = "az-user"
}

variable "vm_distribution" {
  description = "Distribuicao Linux"
  default     = "ubuntu18"
}

variable "vm_size" {
  description = "VM Type"
  default     = "Standard_B2s"
}

variable "vm_public_ip" {
  default     = false
  description = "Adicionar IP Publico?"
}

variable "vm_sg_allow_vnet" {
  description = "Liberar acesso para a rede da VNet?"
  default     = true
}

variable "vm_sg_allow_ssh" {
  description = "Liberar acesso SSH?"
  default     = false
}

variable "vm_custom_data" {
  default = ""
}

variable "vm_data_disk" {
  description = "Adicionar um disco de dados?"
  default     = false
}

variable "vm_data_disk_size" {
  description = "Tamanho do disco de dados"
  default     = "50"
}

variable "vm_availability_set" {
  description = "Availability Sets"
  default     = ""
}

variable "delete_os_disk_on_termination" {
  description = "Remover discos do SO quando der terminate?"
  default     = "true"
}

variable "delete_data_disks_on_termination" {
  description = "Remover discos de dados quando der terminate?"
  default     = "true"
}

variable "images" {
  description = "A map of images to use with virtual_machine"
  type        = "map"

  default = {
    centos73.publisher = "OpenLogic"
    centos73.offer     = "CentOS"
    centos73.sku       = "7-CI"

    ubuntu16.publisher = "Canonical"
    ubuntu16.offer     = "UbuntuServer"
    ubuntu16.sku       = "16.04-LTS"

    ubuntu18.publisher = "Canonical"
    ubuntu18.offer     = "UbuntuServer"
    ubuntu18.sku       = "18.04-LTS"

    debian9.publisher = "credativ"
    debian9.offer     = "Debian"
    debian9.sku       = "9"
  }
}
