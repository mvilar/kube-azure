variable "environment" {
  description = "Sigla de descricao do Ambiente"
}

variable "company" {
  description = "Nome da empresa"
}

variable "vnet" {
  description = "Nome da VNet"
}

variable "region" {
  description = "Regiao da Azure"
}

variable "resource_group" {
  description = "Nome do Resource Group"
}

variable "public_key" {
  description = "Chave de acesso SSH"
}

variable "subnet" {
  description = "Lista de nome das subnets"
}

variable "route_table" {
  description = "Tabela de rota que esta vinculada a subnet"
}

variable "node_count" {
  description = "Quantidade de VMs para nodes"
  default     = 1
}

variable "master_vm_size" {
  description = "Tipo de VM para Master"
  default     = "Standard_B2s"
}

variable "node_vm_size" {
  description = "Tipo de VM para Nodes"
  default     = "Standard_B2s"
}

variable "vm_public_ip" {
  description = "Adicionar IP Publico a VM?"
  default     = false
}

variable "vm_sg_allow_ssh" {
  description = "Liberar acesso SSH para qualquer origem?"
  default     = false
}

variable "vm_data_disk" {
  description = "Adicionar um disco de dados?"
  default     = false
}

variable "vm_data_disk_size" {
  description = "Tamanho do disco de dados"
  default     = "50"
}

variable "azure_tenantId" {
  description = "Azure Tenant ID"
}

variable "azure_subscriptionId" {
  description = "Azure Subscription ID"
}

variable "azure_aadClientId" {
  description = "Application Client ID"
}

variable "azure_aadClientSecret" {
  description = "Application Client Secret"
}

variable "kubernetes_version" {
  description = "Versao do Kubernetes a ser utilizada"
}
